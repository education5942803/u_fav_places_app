import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:u_fav_places_app/models/fav_place_model.dart';

class FavsNotifier extends StateNotifier<List<FavPlaceModel>> {
  FavsNotifier() : super(const []);

  void addPlace(FavPlaceModel place) {
    state = [place, ...state];
  }
}

final favsProvider = StateNotifierProvider<FavsNotifier, List<FavPlaceModel>>(
  (ref) => FavsNotifier(),
);
