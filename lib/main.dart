import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:u_fav_places_app/screens/fav_places_screen.dart';
import 'package:u_fav_places_app/ui/core/ui_setup.dart' as UI_SETUP;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    const ProviderScope(
      child: UFavPlacesApp(),
    ),
  );
}

class UFavPlacesApp extends StatelessWidget {
  const UFavPlacesApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fav Places',
      theme: UI_SETUP.theme,
      home: const FavPlacesScreen(),
    );
  }
}
