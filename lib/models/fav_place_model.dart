import 'dart:io';

class FavPlaceModel {
  const FavPlaceModel({
    required this.id,
    required this.name,
    required this.image,
  });

  final dynamic id;
  final String name;
  final File image;
}
