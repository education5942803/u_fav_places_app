import 'package:flutter/material.dart';
import 'package:u_fav_places_app/models/fav_place_model.dart';
import 'package:u_fav_places_app/screens/fav_place_detail_screen.dart';

class FavPlacesList extends StatelessWidget {
  const FavPlacesList(this.favPlaces, {super.key});

  final List<FavPlaceModel> favPlaces;

  @override
  Widget build(BuildContext context) {
    void onTapPlace(FavPlaceModel favPlace) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (_) => FavPlaceDetailScreen(favPlace),
        ),
      );
    }

    return ListView.builder(
      itemCount: favPlaces.length,
      itemBuilder: (_, index) {
        final FavPlaceModel currFavPlace = favPlaces[index];

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
          child: Dismissible(
            key: ValueKey(currFavPlace.id),
            onDismissed: (_) {},
            child: ListTile(
              leading: CircleAvatar(
                radius: 26.0,
                backgroundImage: FileImage(currFavPlace.image),
              ),
              title: Text(
                currFavPlace.name,
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontSize: 18.0,
                    ),
              ),
              onTap: () => onTapPlace(currFavPlace),
            ),
          ),
        );
      },
    );
  }
}
