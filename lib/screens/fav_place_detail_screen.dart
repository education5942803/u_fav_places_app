import 'package:flutter/material.dart';
import 'package:u_fav_places_app/models/fav_place_model.dart';

class FavPlaceDetailScreen extends StatelessWidget {
  const FavPlaceDetailScreen(this.favPlace, {super.key});

  final FavPlaceModel favPlace;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(favPlace.name),
      ),
      body: Stack(
        children: [
          Image.file(
            favPlace.image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
        ],
      ),
    );
  }
}
