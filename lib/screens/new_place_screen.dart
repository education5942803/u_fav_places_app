import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:u_fav_places_app/models/fav_place_model.dart';
import 'package:u_fav_places_app/providers/favs_provider.dart';
import 'package:u_fav_places_app/widgets/image_input.dart';

class NewPlaceScreen extends ConsumerWidget {
  const NewPlaceScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formKey = GlobalKey<FormState>();
    File? selectedImage;

    String inputedTitle = '';

    void addPlace() {
      if (!formKey.currentState!.validate()) {
        return;
      }

      formKey.currentState!.save();

      if (selectedImage == null) {
        return;
      }

      ref.read(favsProvider.notifier).addPlace(
            FavPlaceModel(
              id: ValueKey(inputedTitle),
              name: inputedTitle,
              image: selectedImage!,
            ),
          );

      Navigator.of(context).pop();
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Add new Place'),
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(label: Text('Title')),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      color: Theme.of(context).colorScheme.onBackground,
                      fontSize: 18.0,
                    ),
                validator: (value) {
                  final isErrorTriggered =
                      value == null || value.trim().isEmpty;

                  return isErrorTriggered
                      ? 'Field must not be empty. Please, input the name.'
                      : null;
                },
                onSaved: (value) => inputedTitle = value!,
              ),
              const SizedBox(height: 28.0),
              ImageInput(onPickImage: (image) => selectedImage = image),
              const SizedBox(height: 28.0),
              ElevatedButton.icon(
                onPressed: addPlace,
                icon: const Icon(Icons.add),
                label: const Text('Add Place'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
