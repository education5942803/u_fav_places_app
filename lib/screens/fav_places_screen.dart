import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:u_fav_places_app/models/fav_place_model.dart';
import 'package:u_fav_places_app/providers/favs_provider.dart';
import 'package:u_fav_places_app/screens/new_place_screen.dart';
import 'package:u_fav_places_app/widgets/fav_places_list.dart';

class FavPlacesScreen extends ConsumerWidget {
  const FavPlacesScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<FavPlaceModel> favPlaces = ref.watch(favsProvider);

    void addPlace() {
      Navigator.of(context).push<FavPlaceModel>(
        MaterialPageRoute(
          builder: (_) => const NewPlaceScreen(),
        ),
      );
    }

    final Widget contentWidget = favPlaces.isEmpty
        ? Center(
            child: Text(
              'No favourite places added yet...',
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    color: Theme.of(context).colorScheme.onBackground,
                    fontSize: 24.0,
                  ),
            ),
          )
        : FavPlacesList(favPlaces);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Places'),
        actions: [
          IconButton(
            onPressed: addPlace,
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: contentWidget,
    );
  }
}
